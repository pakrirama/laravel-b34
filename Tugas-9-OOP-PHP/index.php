<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>OOP PHP</title>
</head>
<body>
    <?php
        ini_set('display_errors', '1');
        ini_set('display_startup_errors', '1');
        error_reporting(E_ALL); 

        require_once("animal.php");
        require_once("frog.php");
        require_once("ape.php");
        

        $sheep = new Animal("shaun");
        
            echo $sheep->name; // "shaun"
            echo $sheep->legs; // 4
            echo $sheep->cold_blooded; // "no"
            echo "<br>";
                     
        $kodok = new Frog("buduk");
            $kodok->jump() ; // "hop hop"   
            
        $sungokong = new Ape("kera sakti");
            $sungokong->yell(); // "Auooo"
            

       



        //===================================================================

         // Menggunakan get dan set 
         // $sheep = new Animal("shaun");
        
        //     //Untuk Memanggil tanpa get && set
        //     // echo $sheep->name; // "shaun"
        //     // echo $sheep->legs; // 4
        //     // echo $sheep->cold_blooded; // "no"

        //     // $sheep->set_name("Sheep");
        //     $sheep->set_legs(4);
        //     $sheep->set_cold_blooded("No");

        //     // echo $sheep->name;
        //     // echo $sheep->get_name();
        //     echo $sheep->get_legs();
        //     echo $sheep->get_cold_blooded();
        //     echo "<br>";

        // $kodok = new Frog("buduk");
        //     $kodok->set_legs(4);
        //     $kodok->set_cold_blooded("No");

        //     echo $kodok->get_legs();
        //     echo $kodok->get_cold_blooded();
        //     echo $kodok->jump() ; // "hop hop"
        //     echo "<br>";

        // $sungokong = new Ape("kera sakti");

        //     $sungokong->set_legs(2);
        //     $sungokong->set_cold_blooded("No");

        //     echo $sungokong->get_legs();
        //     echo $sungokong->get_cold_blooded();
        //     echo $sungokong->yell();
        //     echo "<br>";

    ?>
</body>
</html>