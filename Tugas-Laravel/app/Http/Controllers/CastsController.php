<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;


class CastsController extends Controller
{
    public function create(){
        return view('cast.create');
    }

    public function store(Request $request){
        // dd($request->all());
        $request->validate([
            'nama' => 'required|min:5',
            'umur' => 'required',
            'bio' => 'required',
        ],
        [
            'nama.required' => 'Nama harus diisi!',
            'nama.min' => 'Nama harus lebih dari 5 karakter!',
            'umur.required' => 'Umur harus diisi!',
            'bio.required' => 'Bio harus diisi!',
        ]
        );
        DB::table('casts')->insert([
            'nama' => $request['nama'],
            'umur' => $request['umur'],
            'bio' => $request['bio'],
        ]); 

        return redirect('/cast');
    }
    
    public function index(){
        $casts = DB::table('casts')->get(); 
        return view('cast.index',compact('casts'));
    }

    public function show($id){
        $casts = DB::table('casts')->where('id',$id)->first();
        
        return view('cast.show',compact('casts'));

    }
    
    public function edit($id){
        $casts = DB::table('casts')->where('id',$id)->first();
        
        return view('cast.edit',compact('casts'));

    }
    
    public function update($id){
        DB::table('casts')->where('id',$id)->update([
            'nama'=>$request['nama'],
            'umur'=>$request['umur'],
            'bio'=>$request['bio']
        ]);
        
        return redirect('/cast');

    }
    
    public function destroy($id){
        DB::table('casts')->where('id',$id)->delete();
        
        return redirect('/cast');

    }
}
