<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function form(){
        return view('register');
    }
   
    public function welcome(){
        return view('welcome');
    }
   
   public function welcome_post(Request $request){
    //    dd($request->all());
    //    dd($request["firstName"]);
    //    var_dump($request["firstName"]);
        $firstName = $request["firstName"];
        $lastName = $request["lastName"];
        return view('welcome',["firstName"=>$firstName,"lastName"=>$lastName]);
   }
}
