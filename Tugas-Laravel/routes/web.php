<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

// Route::get('/home',function(){
//     return view('home');
// });

// Route::get('/register',function(){
//     return view('register');
// });
Route::get('/','HomeController@home');
Route::get('/register','AuthController@form');
Route::get('/welcome','AuthController@welcome');
Route::post('/welcome','AuthController@welcome_post');

Route::get('/master',function(){
    return view('adminlte.master');
});

Route::get('/table',function(){
    return view('items.table');
});
Route::get('/data-table',function(){
    return view('items.data-table');
});

//CRUD Casts
//Create
Route::get('/cast/create', 'CastsController@create');
//Store to DB
Route::post('/cast', 'CastsController@store');

//Read
//show to List
Route::get('/cast', 'CastsController@index');
//show detail
Route::get('/cast/{casts_id}', 'CastsController@show');

//Update
Route::get('/cast/{casts_id}/edit', 'CastsController@edit');
//Update
Route::put('/cast/{casts_id}', 'CastsController@update');

//DELETE
Route::delete('/cast/{casts_id}', 'CastsController@destroy');

