@extends('adminlte.master')

@section('content')

<form method='POST' action='/cast'>
    @csrf
    <h1>Create Cast</h1>
  <div class="form-group">
    <label for="exampleInputEmail1">Nama</label>
    <input type="text" class="form-control" name="nama" placeholder="Nama">
  </div>
  <div class="form-group">
    <label for="exampleInputPassword1">Umur</label>
    <input type="number" class="form-control" name="umur" placeholder="Umur">
  </div>
  <div class="form-group">
    <label for="exampleInputPassword1">Bio</label>
    <textarea class="form-control" name="bio" placeholder="Bio" rows=15></textarea>
  </div>
  <button type="submit" class="btn btn-primary">Submit</button>
</form>

@endsection