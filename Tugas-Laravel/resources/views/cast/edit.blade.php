@extends('adminlte.master')

@section('content')

<form method='POST' action='/cast/{{$casts->id}}'>
    @csrf
    @method('put')
    <h1>Create Cast</h1>
  <div class="form-group">
    <label for="exampleInputEmail1">Nama</label>
    <input type="text" class="form-control" name="nama" placeholder="Nama" value="{{$casts->nama}}">
  </div>
  <div class="form-group">
    <label for="exampleInputPassword1">Umur</label>
    <input type="number" class="form-control" name="umur" placeholder="Umur" value="{{$casts->umur}}">
  </div>
  <div class="form-group">
    <label for="exampleInputPassword1">Bio</label>
    <textarea class="form-control" name="bio" placeholder="Bio" rows=15 >{{$casts->bio}}</textarea>
  </div>
  <button type="submit" class="btn btn-primary">Submit</button>
</form>

@endsection