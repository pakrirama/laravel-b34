@extends('adminlte.master')

@section('content')
    <div class="card mt-2">
        <div class="card-header">
            <h1>Cast Detail </h1>
        </div>
        <div class="card-body">
            <h3 class="card-text">{{$casts->nama}}</h3>
            <br>
            <h3 class="card-text">Umur:{{$casts->umur}}</h3>
            <h3 class="card-text">Bio : {{$casts->bio}}</h3>
            <a href="/cast" class="btn btn-primary"> Back to Cast List</a>
        </div>
    </div>
@endsection