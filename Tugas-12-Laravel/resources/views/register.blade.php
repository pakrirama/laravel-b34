<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Form Latihan</title>
</head>
<body>
    <form action="/welcome" method="post" >
        @csrf
        <h1>Buat Account Baru</h1>
        <h3>Sign Up Form</h3>
        <label for="first-name">First Name :</label> <br>
        <input type="text" id="first-name" name ="firstName"> <br>
        <br>
        <label for="last-name">Last Name :</label> <br>
        <input type="text" id="last-name" name="lastName"><br>
        <br>
        <label for="gender">Gender :</label><br>
        <input type="radio" name="gender" value="male">Male<br>
        <input type="radio" name="gender" value="female">Female<br>
        <input type="radio" name="gender" value="other">Other<br>
        <br>
        <label for="nationality">Nationality :</label><br>
        <select id="nationality">
            <option value="indonesian" name="nationality">Indonesian</option>
            <option value="singaporean" name="nationality">Singaporean</option>
            <option value="malaysian" name="nationality">Malaysian</option>
            <option value="australian" name="nationality">Australian</option>
        </select> <br>
            <br>
            <label for="language">Language Spoken :</label><br><br>
            <input type="checkbox" value="bahasa indonesia" name ="langauage">Bahasa Indonesia <br>
            <input type="checkbox" value="english" name ="langauage">English <br>
            <input type="checkbox" value="other" name="language">Other <br>
         </select>
        <br>
        <label for="bio">Bio :</label><br>
        <textarea  id="bio" cols="30" rows="10" name="bio"></textarea><br>
        <br>
        <input type="submit" value="Submit">
    </form>
</body>
</html>